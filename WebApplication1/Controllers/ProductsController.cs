﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ProductsController : Controller
    {
        // GET: Products
        public ActionResult Index()
        {
            try
            {
                using (ShopDBEntities db = new ShopDBEntities())
                {
                    return View(db.Products.ToList());
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("error al iniciar pagina", e);
                return View();
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Products product)
        {
            if (!ModelState.IsValid)
                return View();

            try
            {
                using (ShopDBEntities db = new ShopDBEntities())
                {
                    db.Products.Add(product);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("error al agregar producto", e);
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            try
            {
                using (ShopDBEntities db = new ShopDBEntities())
                {
                    Products p = db.Products.Find(id);
                    return View(p);
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("error al editar usuario", e);
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Products prod)
        {
            try
            {
                using (ShopDBEntities db = new ShopDBEntities())
                {
                    Products p = db.Products.Find(prod.id);
                    p.name = prod.name;
                    p.price = prod.price;
                    p.description = prod.description;
                    p.image = prod.image;
                    p.quantity = prod.quantity;
                    p.category_id = prod.category_id;
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("error al editar usuario", e);
                return View();
            }
        }

        public ActionResult Details(int id)
        {
            try
            {
                using (ShopDBEntities db = new ShopDBEntities())
                {
                    Products u = db.Products.Find(id);
                    return View(u);
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("error al mostrar detalle del producto", e);
                return View();
            }
        }

        public ActionResult delete(int id)
        {
            try
            {
                using (ShopDBEntities db = new ShopDBEntities())
                {
                    Products p = db.Products.Find(id);
                    db.Products.Remove(p);
                    db.SaveChanges();
                    return RedirectToAction("index");
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("error al eliminar el producto", e);
                return RedirectToAction("index");
            }
        }
    }
}