﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers.api
{
    public class ProductsController : ApiController
    {
        // GET api/Products
        public List<ProductsEX> Get()
        {
            List<ProductsEX> l = new List<ProductsEX>();
            using (ShopDBEntities db = new ShopDBEntities())
            {
                foreach (Products p in  db.Products)
                {
                    ProductsEX c = new ProductsEX();
                    c.id = p.id;
                    c.name = p.name;
                    c.price = p.price;
                    c.description = p.description;
                    c.image = p.image;
                    c.quantity = p.quantity;
                    c.category = p.Categories.name;

                    l.Add(c);
                };
                
                return l;
            }
        }

        // POST: api/products/search
        
        [Route("api/products/search")]
        [HttpPost]
        public List<ProductsEX> search( [FromBody]string value )
        {
            List<ProductsEX> l = new List<ProductsEX>();
            using (ShopDBEntities db = new ShopDBEntities())
            {
                foreach (Products p in db.Products.Where( Products => Products.name == value ))
                {
                    ProductsEX c = new ProductsEX();
                    c.id = p.id;
                    c.name = p.name;
                    c.price = p.price;
                    c.description = p.description;
                    c.image = p.image;
                    c.quantity = p.quantity;
                    c.category = p.Categories.name;

                    l.Add(c);
                };

                return l;
            }
        }

        // GET: api/products/filterByCategory
        
        public List<ProductsEX> filterByCategory(int id)
        {
            List<ProductsEX> l = new List<ProductsEX>();
            using (ShopDBEntities db = new ShopDBEntities())
            {
                foreach (Products p in db.Products.Where(Products => Products.category_id == id))
                {
                    ProductsEX c = new ProductsEX();
                    c.id = p.id;
                    c.name = p.name;
                    c.price = p.price;
                    c.description = p.description;
                    c.image = p.image;
                    c.quantity = p.quantity;
                    c.category = p.Categories.name;

                    l.Add(c);
                };

                return l;
            }
        }
    }


}
